//
//  Question.m
//  UsingStackoverflowAPI
//
//  Created by Vladislav on 10.12.13.
//  Copyright (c) 2013 High Tech Center. All rights reserved.
//

#import "QuestionDataModel.h"

@interface QuestionDataModel ()

@property (nonatomic, readwrite) NSString *questionId;
@property (nonatomic, readwrite) NSString *author;
@property (nonatomic, readwrite) NSDate   *date;
@property (nonatomic, readwrite) NSNumber *answCount;
@property (nonatomic, readwrite) NSString *title;
@property (nonatomic, readwrite) NSString *body;

@end

@implementation QuestionDataModel

+ (id)initWithAuthor:(NSString *)author date:(NSDate *)date answCount:(NSNumber *)count
               title:(NSString *)title questionId:(NSString *)questionId body:(NSString *)body {
    return [[self alloc] initWithAuthor:author date:date answCount:count title:title questionId:questionId body:body];
}

- (id)initWithAuthor:(NSString *)author date:(NSDate *)date answCount:(NSNumber *)count
               title:(NSString *)title questionId:(NSString *)questionId body:(NSString *)body {
    if ((self = [super init])) {
        if ([author isKindOfClass:[NSString class]]) {
            _author = author;
        }
        if ([date isKindOfClass:[NSDate class]]) {
            _date   = date;
        }
        if ([count isKindOfClass:[NSNumber class]] || [count isKindOfClass:[NSString class]]) {
            _answCount = [NSNumber numberWithInteger:[count integerValue]];
        }
        
        if ([title isKindOfClass:[NSString class]]) {
            _title = title;
        }
        if ([questionId isKindOfClass:[NSString class]]) {
            _questionId = questionId;
        }
        if ([body isKindOfClass:[NSString class]]) {
            _body = body;
        }
    }
    return self;
}

@end

