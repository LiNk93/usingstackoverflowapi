//
//  NextViewController.h
//  UsingStackoverflowAPI
//
//  Created by Vladislav on 06.12.13.
//  Copyright (c) 2013 High Tech Center. All rights reserved.
//

#import <UIKit/UIKit.h>

@class QuestionDataModel;

@interface DetailViewController : UITableViewController

@property (nonatomic) QuestionDataModel *question;

@end
