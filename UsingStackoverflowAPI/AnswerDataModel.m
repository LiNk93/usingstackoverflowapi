//
//  Answer.m
//  UsingStackoverflowAPI
//
//  Created by Vladislav on 10.12.13.
//  Copyright (c) 2013 High Tech Center. All rights reserved.
//

#import "AnswerDataModel.h"

@interface AnswerDataModel ()

@property (nonatomic, readwrite) NSString *author;
@property (nonatomic, readwrite) NSDate   *date;
@property (nonatomic, readwrite) NSInteger score;
@property (nonatomic, readwrite) NSMutableString  *body;
@property (nonatomic, readwrite) BOOL *isAccepted;

@end

@implementation AnswerDataModel

+ (id)initWithTitle:(NSString *)author date:(NSDate *)date score:(NSInteger)score
                 body:(NSMutableString *)body isAccepted:(BOOL *)isAccepted {
    return [[self alloc] initWithTitle:author date:date score:score body:body isAccepted:isAccepted];
}

- (id)initWithTitle:(NSString *)author date:(NSDate *)date score:(NSInteger)score
              body:(NSMutableString *)body isAccepted:(BOOL *)isAccepted {
    if ((self = [super init])) {
        if ([author isKindOfClass:[NSString class]]) {
            _author = author;
        }
        if ([date isKindOfClass:[NSDate class]]) {
            _date   = date;
        }
        _score  = score;
        if ([body isKindOfClass:[NSString class]]) {
            _body = body;
        }
        _isAccepted = isAccepted;
    }
    return self;
}

@end
