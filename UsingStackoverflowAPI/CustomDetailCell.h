//
//  CustomDetailCell.h
//  UsingStackoverflowAPI
//
//  Created by Vladislav on 06.12.13.
//  Copyright (c) 2013 High Tech Center. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomDetailCell : UITableViewCell

@property (nonatomic) IBOutlet UILabel *authorDetLabel;
@property (nonatomic) IBOutlet UILabel *questionDetLabel;
@property (nonatomic) IBOutlet UILabel *dateDetLabel;
@property (nonatomic) IBOutlet UILabel *numDetLabel;

@end
