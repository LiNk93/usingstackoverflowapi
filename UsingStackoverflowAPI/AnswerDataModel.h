//
//  Answer.h
//  UsingStackoverflowAPI
//
//  Created by Vladislav on 10.12.13.
//  Copyright (c) 2013 High Tech Center. All rights reserved.
//

#import <Foundation/Foundation.h>

//Класс для хранения данных ответа

@interface AnswerDataModel : NSObject 

@property (nonatomic, readonly) NSString *author;
@property (nonatomic, readonly) NSDate   *date;
@property (nonatomic, readonly) NSInteger score;
@property (nonatomic, readonly) NSMutableString  *body;
@property (nonatomic, readonly) BOOL *isAccepted;


+ (id)initWithTitle:(NSString *)author date:(NSDate *)date score:(NSInteger)score
              body:(NSMutableString *)body isAccepted:(BOOL *)isAccepted;

- (id)initWithTitle:(NSString *)author date:(NSDate *)date score:(NSInteger)score
              body:(NSMutableString *)body isAccepted:(BOOL *)isAccepted;

@end
