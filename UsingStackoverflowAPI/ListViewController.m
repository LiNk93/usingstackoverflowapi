//
//  ViewController.m
//  UsingStackoverflowAPI
//
//  Created by Vladislav on 04.12.13.
//  Copyright (c) 2013 High Tech Center. All rights reserved.
//

#import "ListViewController.h"
#import "CustomCell.h"
#import "DetailViewController.h"
#import "QuestionDataModel.h"
#import "HTTPJSONRequestHandler.h"
#import "DataTransfersDelegate.h"

const NSTimeInterval CHECK_CHANGE = 60 * 5;             // Константа для времени изменения - 5 минут
const NSTimeInterval SECONDS_PER_DAY = 24 * 60 * 60;    // Количество секунд в одном дне

@interface ListViewController () <UITableViewDataSource, UITableViewDelegate, UIPickerViewDelegate, UIPickerViewDataSource, DataTransfersDelegate>

//outlets
@property (weak, nonatomic) IBOutlet UIView *viewGroup;
@property (weak, nonatomic) IBOutlet UITableView *table;
@property (weak, nonatomic) IBOutlet UIPickerView *tagPicker;
//все остальное
@property (nonatomic) NSArray *tags;
@property (nonatomic) NSMutableArray *questions;
@property (nonatomic) NSInteger numberOfPage;
@property (nonatomic) NSString  *selectedTag;
@property (nonatomic) NSMutableData *responseData;
@property (nonatomic) BOOL dataIsUpdated;
@property (nonatomic) UIRefreshControl *refreshControl;
@property (nonatomic) HTTPJSONRequestHandler *object;

//Для обработки ошибок (возможно временные)
@property (nonatomic) NSMutableArray *errors;

- (IBAction)closeTagSelector:(UIButton *)sender;
- (IBAction)openTagSelector:(UIButton *)sender;
- (void)getData;
- (void)saveData:(NSMutableArray *)data toFileWithName:(NSString *)filename;
- (void)loadData:(NSString *)filename;
@end

@implementation ListViewController

- (void)dataDownloaded:(id)sender {
    NSArray *items = self.object.data;
    
    if (self.numberOfPage == 1) {
        NSLog(@"Init arrays");
        [self setQuestions:[[NSMutableArray alloc] init]]; //для хранения объектов класса (хранит выводимые поля вопроса)
    }
    
    [items enumerateObjectsUsingBlock: ^(id obj, NSUInteger idx, BOOL *stop)  {
        [self.questions addObject:[[QuestionDataModel alloc] initWithAuthor:(NSString *) [[obj objectForKey:@"owner"] objectForKey:@"display_name"]
                                                                            date:[NSDate dateWithTimeIntervalSince1970:[[obj  objectForKey:@"last_activity_date"] doubleValue]]
                                                                       answCount:(NSNumber *) [obj objectForKey:@"answer_count"]
                                                                           title:(NSString *) [obj objectForKey:@"title"]
                                                                      questionId:(NSString *) [[obj objectForKey:@"question_id"] stringValue]
                                                                            body:(NSString *) [obj objectForKey:@"body"]]
         ];
    }];
    
    NSLog(@"---------------------------------------------");
    NSLog(@"Tag: %@ | Count : %lu | Page : %ld", self.selectedTag, (unsigned long)[self.questions count], (long)self.numberOfPage);
    NSLog(@"---------------------------------------------");
    [self setDataIsUpdated:NO];
    [self saveData:self.questions toFileWithName:self.selectedTag];
    NSLog(@"Tag: %@ | SAVED!", self.selectedTag);
    
    [self.table reloadData];
    
    self.navigationItem.title = self.selectedTag;
}


#pragma mark - ObjectProperties methods

- (void)didFail:(id)sender {
    NSLog(@"ERROR");
}

- (void)getData {
    NSLog(@"Get Data!");
    [self setDataIsUpdated:YES];
    NSString *urlStr = [[NSString alloc] initWithFormat:@"https://api.stackexchange.com/2.1/questions?page=%ld&pagesize=50&order=desc&sort=activity&tagged=%@&site=stackoverflow&filter=!-.CabyPaznWF", (long)self.numberOfPage, self.selectedTag];
    
    [self setObject:[HTTPJSONRequestHandler getDataItems:urlStr delegate:self]];
}


#pragma mark - Actions

- (NSString *)searchPathToDirectory {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString *cachePath = [paths firstObject];
    NSString *filepath = [cachePath stringByAppendingPathComponent:[[NSString alloc] initWithFormat:@"%@", self.selectedTag]];
    return filepath;
}

- (IBAction)openTagSelector:(UIButton *)sender {
    if ( self.viewGroup.frame.origin.y < self.view.frame.size.height ) {
        CGRect newViewGroupFrame = self.viewGroup.frame;
        newViewGroupFrame.origin.y = self.view.frame.size.height;
        [UIView animateWithDuration:0.5 animations:^{   //Анимация
            self.viewGroup.frame = newViewGroupFrame;}];
    }
    else {
        CGRect newViewGroupFrame = self.viewGroup.frame;
        newViewGroupFrame.origin.y = self.view.frame.size.height - newViewGroupFrame.size.height;
        [UIView animateWithDuration:0.5 animations:^{   //Анимация
            self.viewGroup.frame = newViewGroupFrame;}];
    }
}

- (NSTimeInterval)сheckTimeChangeFile {
    NSDate *fileModificationDate = [[[NSFileManager defaultManager] attributesOfItemAtPath:[self searchPathToDirectory] error:NULL] fileModificationDate];
    NSTimeInterval change = [[NSDate date] timeIntervalSinceDate:fileModificationDate];
    
    NSLog(@"%f - date modification", (change));
    return change;
}

- (IBAction)closeTagSelector:(UIButton *)sender {
    self.navigationItem.title = self.selectedTag;
    
    [self setNumberOfPage:1];
    
    [self.questions removeAllObjects];
    
    if ( [self сheckTimeChangeFile] < CHECK_CHANGE )
    {
        [self loadData:self.selectedTag];
    }
    else [self getData];
    
    [self.table reloadData];

    CGRect newViewGroupFrame = self.viewGroup.frame;
    newViewGroupFrame.origin.y = self.view.frame.size.height;
    [UIView animateWithDuration:0.5 animations:^{   //Анимация
        self.viewGroup.frame = newViewGroupFrame;}];
}


#pragma mark - Work with data

// тут рефрешатся данные
- (void)refreshData:(id)sender
{
    NSLog(@"Refreshing");
    [self setNumberOfPage:1];
    
    //[self.questions removeAllObjects];
    
    [self getData];
    
    [self.table reloadData];
    
    [(UIRefreshControl *)sender endRefreshing];
}


- (NSString *)smartDate:(NSDate *)date
{
    
    NSString *newDate = nil;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
    NSTimeInterval interval = [[NSDate date] timeIntervalSinceDate:date];
    
    if (interval < 60) {
        newDate = [NSString stringWithFormat:@"%f seconds ago", interval];
    }
    else
      if (interval < 60 * 60) {
        newDate = [NSString stringWithFormat:@"%ld minutes  ago", (long) ( interval / 60 )];
      }
      else
          if (interval < 60 * 60 * 2) {
              newDate = [NSString stringWithFormat:@"one hour ago"];
          }
          else
              if ( interval <  SECONDS_PER_DAY ) {
                  newDate = [NSString stringWithFormat:@"%ld hours ago", (long) ( interval / 60 / 60 )];
              }
              else
                  if ( ( interval > SECONDS_PER_DAY ) ) {
                      newDate = [NSString stringWithFormat:@"Yesterday"];
                  }
                  else (newDate = [dateFormatter stringFromDate:date]);
    
    return newDate;
}

- (NSMutableArray *)getFileData
{
    NSError *err = nil;
    NSMutableArray *farray;
    NSString *myPath = [self searchPathToDirectory];
    if([[NSFileManager defaultManager] fileExistsAtPath:myPath])
    {
        farray = [NSMutableArray arrayWithContentsOfFile:myPath];
        if(err) NSLog(@"getFileData() - ERROR: %@", [err localizedDescription]);
    }
    else
    {
        NSLog(@"getFileData() - ERROR: This file '%@' does not exist", myPath);
    }
    return farray;
}

- (void)loadData:(NSString *)filename
{
    [self.questions removeAllObjects];
    
    NSLog(@"Tag: %@ Loaded from file!", filename);
    
    NSLog(@"---------------------------------------------");
    NSLog(@"Tag: %@ | Count : %lu | Page : %ld", self.selectedTag, (unsigned long)[self.questions count], (long)self.numberOfPage);
    NSLog(@"---------------------------------------------");
    
    [[self getFileData] enumerateObjectsUsingBlock: ^(id obj, NSUInteger idx, BOOL *stop) {
        [self.questions addObject:[[QuestionDataModel alloc] initWithAuthor:[obj objectForKey:@"author"]
                                                                        date:[obj objectForKey:@"date"]
                                                                   answCount:(NSNumber *)[obj objectForKey:@"count"]
                                                                       title:[obj objectForKey:@"title"]
                                                                  questionId:[obj objectForKey:@"identifier"]
                                                                        body:[obj objectForKey:@"body"]]];
    }];
    [self setNumberOfPage:([self.questions count] / 50)];
    
    [self.table reloadData];
}

- (void)saveData:(NSMutableArray *)data toFileWithName:(NSString *)filename
{
    NSError *err = nil;
    
    NSString *myPath = [self searchPathToDirectory];
    
    NSMutableArray *newArray = [[NSMutableArray alloc] init];
    //self.questions
    [data enumerateObjectsUsingBlock: ^(id obj, NSUInteger idx, BOOL *stop)  {
        [newArray addObject:[[NSDictionary alloc] initWithObjectsAndKeys: [obj author], @"author", [obj date], @"date", [obj answCount], @"count", [obj title], @"title",
                             [obj questionId], @"identifier", [obj body], @"body", nil]];
    }];
    
    [newArray writeToFile:myPath atomically:YES];
    
    if([[NSFileManager defaultManager] fileExistsAtPath:myPath])
    {
        if(err) NSLog(@"getFileData() - ERROR: %@", [err localizedDescription]);
    }
    else
    {
        NSLog(@"getFileData() - ERROR: This file '%@' does not exist", myPath);
    }

}


#pragma mark - Life Cycle of ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    NSArray *array = @[@"Objective-C", @"iOS", @"XCode", @"Cocoa-touch", @"iPhone"];
    
    UIRefreshControl *refresh = [[UIRefreshControl alloc] init];
    refresh.tintColor = [UIColor grayColor];
    [refresh addTarget:self action:@selector(refreshData:) forControlEvents:UIControlEventValueChanged];
    [self.table addSubview:refresh];
    // Configure View Controller
    [self setRefreshControl:refresh];
    
    // all other
    [self setTags:array];
    [self setSelectedTag:@"Objective-C"];
    [self setNumberOfPage:1];
    [self setDataIsUpdated:NO];
    [self getData];
}


#pragma mark - Picker Data Source Methods

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return [self.tags count];
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    self.selectedTag = [self.tags objectAtIndex:row];
}


#pragma mark - Picker Delegate Methods

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return [self.tags objectAtIndex:row];
}


#pragma mark - Table View Data Source Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.questions count] + 1;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ( [indexPath row] == ( [self.questions count] - 1) ) {
        if (!self.dataIsUpdated) {
            [self setNumberOfPage:self.numberOfPage++];
            [self getData];
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger row = [indexPath row];
    
    static NSString *cellID = @"QuestionSummaryCellID";
    
    if ( row == ( [self.questions count] ) ) {
        
        static NSString *cellINDID = @"ActivityIND";

        UITableViewCell *cellIND = [tableView dequeueReusableCellWithIdentifier:cellINDID];
        [(UIActivityIndicatorView *)[cellIND.contentView viewWithTag:1] startAnimating];
        return cellIND;
    }
    
    CustomCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];
    
    cell.authorLabel.text   = [[self.questions objectAtIndex:row] author];
    cell.dateLabel. text    = [self smartDate:[[self.questions objectAtIndex:row] date]];
    cell.numLabel.text      = [[NSString alloc] initWithFormat:@"%ld", (long)[[[self.questions objectAtIndex:row] answCount] integerValue]];
    cell.questionLabel.text = [[self.questions objectAtIndex:row] title];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}


#pragma mark - Table View Delegate Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger row = [indexPath row];
    if (row == [self.questions count]) {
        return;
    }
    DetailViewController *detailController = [self.storyboard instantiateViewControllerWithIdentifier:@"DetailController"];
    [self.navigationController pushViewController:detailController animated:YES];
    // передача объекта из нужной ячейки таблицы
    [detailController setQuestion:self.questions[row]];
}

@end
