//
//  main.m
//  UsingStackoverflowAPI
//
//  Created by Vladislav on 04.12.13.
//  Copyright (c) 2013 High Tech Center. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
