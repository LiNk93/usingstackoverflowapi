//
//  Question.h
//  UsingStackoverflowAPI
//
//  Created by Vladislav on 10.12.13.
//  Copyright (c) 2013 High Tech Center. All rights reserved.
//

#import <Foundation/Foundation.h>

//Класс для хранения данных вопроса

@interface QuestionDataModel : NSObject

@property (nonatomic, readonly) NSString *questionId;
@property (nonatomic, readonly) NSString *author;
@property (nonatomic, readonly) NSDate   *date;
@property (nonatomic, readonly) NSNumber *answCount;
@property (nonatomic, readonly) NSString *title;
@property (nonatomic, readonly) NSString *body;

+ (id)initWithAuthor:(NSString *)author date:(NSDate *)date answCount:(NSNumber *)count
               title:(NSString *)title questionId:(NSString *)questionId body:(NSString *)body;

- (id)initWithAuthor:(NSString *)author date:(NSDate *)date answCount:(NSNumber *)count
               title:(NSString *)title questionId:(NSString *)questionId body:(NSString *)body;

@end
