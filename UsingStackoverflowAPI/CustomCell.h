//
//  CustomCell.h
//  UsingStackoverflowAPI
//
//  Created by Vladislav on 06.12.13.
//  Copyright (c) 2013 High Tech Center. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomCell : UITableViewCell

@property (nonatomic) IBOutlet UILabel *authorLabel;
@property (nonatomic) IBOutlet UILabel *questionLabel;
@property (nonatomic) IBOutlet UILabel *dateLabel;
@property (nonatomic) IBOutlet UILabel *numLabel;

@end
