//
//  ObjectProperties.h
//  UsingStackoverflowAPI
//
//  Created by Vladislav on 14.01.14.
//  Copyright (c) 2014 High Tech Center. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol DataTransfersDelegate <NSObject>

@optional

- (void)dataDownloaded:(id)sender;

- (void)didFail:(id)sender;

@end

