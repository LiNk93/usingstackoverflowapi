//
//  TagParserClass.h
//  UsingStackoverflowAPI
//
//  Created by Vladislav on 27.12.13.
//  Copyright (c) 2013 High Tech Center. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OHASBasicHTMLParser.h"

@interface TagParser : OHASBasicHTMLParser

@end
