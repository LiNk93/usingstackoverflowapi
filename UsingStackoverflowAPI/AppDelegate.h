//
//  AppDelegate.h
//  UsingStackoverflowAPI
//
//  Created by Vladislav on 04.12.13.
//  Copyright (c) 2013 High Tech Center. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
