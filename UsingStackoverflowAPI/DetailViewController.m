//
//  NextViewController.m
//  UsingStackoverflowAPI
//
//  Created by Vladislav on 06.12.13.
//  Copyright (c) 2013 High Tech Center. All rights reserved.
//

#import "DetailViewController.h"
#import "TagParser.h"
#import "NSAttributedString+Attributes.h"
#import "CustomDetailCell.h"
#import "AnswerDataModel.h"
#import "QuestionDataModel.h"
#import "HTTPJSONRequestHandler.h"
#import "DataTransfersDelegate.h"

const CGFloat HEIGHT_CELL_TITLE = 135.f;  //Константа, содержащая высоту заголовка(того что над лэйблом с телом вопроса/ответа)

@interface DetailViewController () <UITableViewDataSource, UITableViewDelegate, DataTransfersDelegate> 

@property (nonatomic) IBOutlet UITableView *table;
@property (nonatomic) NSMutableData *responseData;
@property (nonatomic) NSMutableArray *answers;
@property (nonatomic) HTTPJSONRequestHandler *object;

- (void)GetAnswersData;

@end

@implementation DetailViewController


#pragma mark - ObjectProperties methods

- (void)dataDownloaded:(id)sender
{
    NSArray *items = self.object.data;
    
    [self setAnswers:[[NSMutableArray alloc] init]];
    
    [items enumerateObjectsUsingBlock: ^(id obj, NSUInteger idx, BOOL *stop)  {
        [self.answers addObject:[[AnswerDataModel alloc] initWithTitle:(NSString*) [[obj objectForKey:@"owner"] objectForKey:@"display_name"]
                                                                       date:[NSDate dateWithTimeIntervalSince1970:[[obj objectForKey:@"last_activity_date"] doubleValue]]
                                                                      score:[[obj objectForKey:@"score"] integerValue]
                                                                       body:(NSMutableString *) [obj objectForKey:@"body"]
                                                                 isAccepted:(BOOL *)[[obj objectForKey:@"isAccepted"] boolValue]
                                 ]];
    }];
    
    [self.table reloadData];
}

- (void)didFail:(id)sender
{
    NSLog(@"ERROR");
}


#pragma mark - Work with data

- (void)GetAnswersData
{
    NSString *urlStr = [ [NSString alloc] initWithFormat:@"https://api.stackexchange.com/2.1/questions/%@/answers?order=desc&sort=activity&site=stackoverflow&filter=!9f8L7BVrc",
                        self.question.questionId ];
    NSLog(@"%@", self.question.questionId);
    [self setObject:[HTTPJSONRequestHandler getDataItems:urlStr delegate:self]];
}

- (NSAttributedString *)cutString:(NSMutableString *)newStr {
    //создание строки с атрибутами
    NSMutableAttributedString *attStrRes=[[NSMutableAttributedString alloc] initWithString:newStr];
    
    //Далее следует разобраться с выпилом остальных тегов
    
    [TagParser processMarkupInAttributedString:attStrRes]; //Должен впилить в строку необходимые атрибуты по тегам и выпилить остальные теги
    
    return attStrRes;
}


#pragma mark - Life Cycle of DetailViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self GetAnswersData];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    NSLog(%lu%d"(unsigned long), [self.answers count]);
    return [self.answers count] + 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellID = @"QuestionCellID";
    CustomDetailCell *cell = [tableView dequeueReusableCellWithIdentifier:cellID];

    if (cell == nil) {
        cell = [[CustomDetailCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellID];
    }
    
    NSInteger row = [indexPath row] - 1;
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateStyle:NSDateFormatterMediumStyle];
    
    if ( row == -1)
    {
        cell.authorDetLabel.text =  (NSString *) self.question.author;
        cell.dateDetLabel.text =  (NSString *) [NSString stringWithFormat:@"%@", self.question.date];
        cell.numDetLabel.text = [[NSString alloc] initWithFormat:@"%ld", (long)[self.question.answCount integerValue]];
        cell.questionDetLabel.attributedText = [self cutString:(NSMutableString *)self.question.body];
        NSLog(@"%@", [self cutString:(NSMutableString *)self.question.body]); // Временная фигня, для проверки
        cell.questionDetLabel.numberOfLines = 0;
        [cell.questionDetLabel sizeToFit];
        cell.backgroundColor = [UIColor cyanColor];
        cell.selectionStyle =UITableViewCellSelectionStyleNone;
    }
    else {
        
    cell.authorDetLabel.text = (NSString*) [[self.answers objectAtIndex:row] author];
    cell.dateDetLabel.text = [dateFormatter stringFromDate:[[self.answers objectAtIndex:row] date] ];
    cell.numDetLabel.text = [[NSString alloc] initWithFormat:@"%ld", (long)[[self.answers objectAtIndex:row] score]];
    cell.questionDetLabel.attributedText = [self cutString:(NSMutableString *)[[self.answers objectAtIndex:row] body]];
    cell.questionDetLabel.numberOfLines = 0;
    [cell.questionDetLabel sizeToFit];
    if ([[self.answers objectAtIndex:row] isAccepted]) {
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        }
        cell.backgroundColor = [UIColor whiteColor];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    NSLog(@"return cell");
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {  //Тут необходимо разобраться с изменением размеров ячейки
    NSInteger row = [indexPath row] - 1;
    CGRect rect;
    if ( row == -1)
    {
        NSAttributedString *text = [self cutString:(NSMutableString *)self.question.body]; // Берем строку, т.к. этот метод срабатывает до создания клетки в таблице, а значит клетки с лэйблом еще
                                                                                           // нет(что логично т.к. для ее построения нужна высота)
        rect.size = [text sizeConstrainedToSize:CGSizeMake(280.f, CGFLOAT_MAX)];           // Вычисляем размеры строки в нашем прямоугольнике(ширина которого постоянна == 280)
        
    }
    else {
        NSAttributedString *text = [self cutString:(NSMutableString *)[[self.answers objectAtIndex:row] body]];
        rect.size = [text sizeConstrainedToSize:CGSizeMake(280.f, CGFLOAT_MAX)];
    }
    NSLog(@"return height: %f + %f", HEIGHT_CELL_TITLE, rect.size.height);
    return (HEIGHT_CELL_TITLE + ceilf(rect.size.height)); // К высоте ника автора прибавляется высота тела ответа или вопроса
    // Признаться, работеат когда как
}

@end
