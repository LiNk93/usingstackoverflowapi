//
//  NSURLConnectionClass.h
//  UsingStackoverflowAPI
//
//  Created by Vladislav on 13.01.14.
//  Copyright (c) 2014 High Tech Center. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ListViewController.h"
#import "DataTransfersDelegate.h"

@interface HTTPJSONRequestHandler : NSObject

@property (nonatomic) NSMutableData *responseData;
@property (nonatomic) NSArray *data;
@property (weak, nonatomic) id<DataTransfersDelegate> delegate;

+ (HTTPJSONRequestHandler *)getDataItems:(NSString *)urlStr delegate:(id<DataTransfersDelegate>)delegate;

- (instancetype)initWithDelegate:(id<DataTransfersDelegate>)delegate urlStr:(NSString *)urlStr;

@end