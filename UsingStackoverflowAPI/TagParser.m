//
//  TagParserClass.m
//  UsingStackoverflowAPI
//
//  Created by Vladislav on 27.12.13.
//  Copyright (c) 2013 High Tech Center. All rights reserved.
//

#import "TagParser.h"
#import "NSAttributedString+Attributes.h"

@implementation TagParser

+(NSDictionary *)tagMappings
{
    return [NSDictionary dictionaryWithObjectsAndKeys:
            
            ^NSAttributedString*(NSAttributedString* str, NSTextCheckingResult* match)
            {
                NSRange textRange = [match rangeAtIndex:1];
                if (textRange.length>0)
                {
                    NSMutableAttributedString *foundString = [[NSMutableAttributedString alloc] init];
                    
                    [foundString appendAttributedString:[[NSMutableAttributedString alloc] initWithString:@"\n"]];
                    [foundString appendAttributedString:[[str attributedSubstringFromRange:textRange] mutableCopy]];
                    [foundString setTextBold:YES range:NSMakeRange(0,textRange.length)];
                    
                    UIColor *strColor = [UIColor colorWithRed:200.0/255.0 green:200.0/255.0 blue:200.0/255.0 alpha:1];
                    UIFont  *font = [UIFont fontWithName:@"Courier" size:10.0f];
                    
                    [foundString addAttribute:NSFontAttributeName value:font range:NSMakeRange(1, textRange.length) ];
                    [foundString addAttribute:NSBackgroundColorAttributeName value:strColor range:NSMakeRange(1, textRange.length) ];
                    
                    return foundString;
                } else {
                    return nil;
                }
            }, @"<code>(.+?)</code>", // regular expression
            
            ^NSAttributedString*(NSAttributedString* str, NSTextCheckingResult* match)
            {
                NSRange textRange = [match rangeAtIndex:1];
                if (textRange.length>0)
                {
                    NSMutableAttributedString *foundString = [[NSMutableAttributedString alloc] init];
                    [foundString appendAttributedString:[[NSMutableAttributedString alloc] initWithString:@"\n"]];
                    [foundString appendAttributedString:[[str attributedSubstringFromRange:textRange] mutableCopy]];
                    [foundString setTextIsUnderlined:YES];
                    
                    [foundString appendAttributedString:[[NSMutableAttributedString alloc] initWithString:@"\n"]];
                    return foundString;
                } else {
                    return nil;
                }
            }, @"<br>(.+?)</br>", // regular expression
            
            ^NSAttributedString*(NSAttributedString* str, NSTextCheckingResult* match)
            {
                NSRange textRange = [match rangeAtIndex:1];
                if (textRange.length>0)
                {
                    NSMutableAttributedString* foundString = [[str attributedSubstringFromRange:textRange] mutableCopy];
                    [foundString setTextItalics:YES range:NSMakeRange(0,foundString.length)];
                    [foundString setAttributedString:[[NSMutableAttributedString alloc] initWithString:@""]];
                    return foundString;
                } else {
                    return nil;
                }
            }, @"<(.+?)>", // regular expression
            
            
            ^NSAttributedString*(NSAttributedString* str, NSTextCheckingResult* match)
            {
                NSRange textRange = [match rangeAtIndex:1];
                if (textRange.length>0)
                {
                    NSMutableAttributedString* foundString = [[str attributedSubstringFromRange:textRange] mutableCopy];
                    [foundString setTextItalics:YES range:NSMakeRange(0,foundString.length)];
                    return foundString;
                } else {
                    return nil;
                }
            }, @"<i>(.+?)</i>", // regular expression
            
            ^NSAttributedString*(NSAttributedString* str, NSTextCheckingResult* match)
            {
                NSRange fontNameRange = [match rangeAtIndex:2];
                NSRange fontSizeRange = [match rangeAtIndex:4];
                NSRange textRange = [match rangeAtIndex:5];
                if ((fontNameRange.length>0) && (fontSizeRange.length>0) && (textRange.length>0))
                {
                    NSString* fontName = [str attributedSubstringFromRange:fontNameRange].string;
                    CGFloat fontSize = [str attributedSubstringFromRange:fontSizeRange].string.floatValue;
                    NSMutableAttributedString* foundString = [[str attributedSubstringFromRange:textRange] mutableCopy];
                    [foundString setFontName:fontName size:fontSize];
                    return foundString;
                } else {
                    return nil;
                }
            }, @"<font name=(['\"])(.+?)\\1 size=(['\"])(.+?)\\3>(.+?)</font>", // regular expression
            
            ^NSAttributedString*(NSAttributedString* str, NSTextCheckingResult* match)
            {
                NSRange colorRange = [match rangeAtIndex:2];
                NSRange textRange = [match rangeAtIndex:3];
                if ((colorRange.length>0) && (textRange.length>0))
                {
                    NSString* colorName = [str attributedSubstringFromRange:colorRange].string;
                    UIColor* color = OHUIColorFromString(colorName);
                    NSMutableAttributedString* foundString = [[str attributedSubstringFromRange:textRange] mutableCopy];
                    [foundString setTextColor:color];
                    return foundString;
                } else {
                    return nil;
                }
            }, @"<font color=(['\"])(.+?)\\1>(.+?)</font>", // regular expression
            
            ^NSAttributedString*(NSAttributedString* str, NSTextCheckingResult* match)
            {
                NSRange linkRange = [match rangeAtIndex:2];
                NSRange textRange = [match rangeAtIndex:3];
                if ((linkRange.length>0) && (textRange.length>0))
                {
                    NSString* link = [str attributedSubstringFromRange:linkRange].string;
                    NSMutableAttributedString* foundString = [[str attributedSubstringFromRange:textRange] mutableCopy];
                    [foundString setLink:[NSURL URLWithString:link] range:NSMakeRange(0,textRange.length)];
                    return foundString;
                } else {
                    return nil;
                }
            }, @"<a href=(['\"])(.+?)\\1>(.+?)</a>", // regular expression
            
            nil];
}

@end
