//
//  NSURLConnectionClass.m
//  UsingStackoverflowAPI
//
//  Created by Vladislav on 13.01.14.
//  Copyright (c) 2014 High Tech Center. All rights reserved.
//

#import "HTTPJSONRequestHandler.h"

@interface HTTPJSONRequestHandler () <NSURLConnectionDelegate, DataTransfersDelegate>

@end

@implementation HTTPJSONRequestHandler

//@synthesize delegate;

+ (HTTPJSONRequestHandler *)getDataItems:(NSString *)urlStr delegate:(id<DataTransfersDelegate>)delegate {
    HTTPJSONRequestHandler *object = [[HTTPJSONRequestHandler alloc] initWithDelegate:delegate urlStr:urlStr];
    return object;
}

- (instancetype)initWithDelegate:(id<DataTransfersDelegate>)delegate urlStr:(NSString *)urlStr {
    self = [super init];
    if (self != nil) {
        NSURL *url = [NSURL URLWithString:urlStr];
        NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                               cachePolicy:NSURLCacheStorageNotAllowed
                                                           timeoutInterval:20.0];
        request.HTTPMethod = @"GET";
        
        NSURLConnection *connection = [[NSURLConnection alloc] initWithRequest:request delegate:self startImmediately:YES];
        if (connection)
        {
            NSLog(@"Connection!");
        }
        else
        {

        }

        self.delegate = delegate;
    }
    return self;
}

#pragma mark NSURLConnection Delegate Methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    // A response has been received, this is where we initialize the instance var you created
    // so that we can append data to it in the didReceiveData method
    // Furthermore, this method is called each time there is a redirect so reinitializing it
    // also serves to clear it
    _responseData = [[NSMutableData alloc] init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    // Append the new data to the instance variable you declared
    [_responseData appendData:data];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    // The request is complete and data has been received
    // You can parse the stuff in your instance variable now
    id result = [NSJSONSerialization JSONObjectWithData:_responseData options:0 error:nil];
    
    NSArray *items = [result objectForKey:@"items"];
    NSArray *errors = [result objectForKey:@"errors"];
    
    if ([[errors firstObject] objectForKey:@"error_name"]) {
        NSLog(@"Принимаемые данные содержат ошибку!");
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:(@"%@", [[errors firstObject] objectForKey:@"error_name"])
//                                                        message:(@"%@", [[errors firstObject] objectForKey:@"description"])
//                                                       delegate:self
//                                              cancelButtonTitle:@"Cancel"
//                                              otherButtonTitles:@"OK", nil];
//        [alert show];
    }
    else
        NSLog(@"Ошибок в принимаемых данных нет.");
    [self setData:items];
    [self.delegate dataDownloaded:self];
    //тут нужно как-то обрабатывать время срабатывания
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    // The request has failed for some reason!
    // Check the error var
    [self.delegate didFail:self];
    NSLog(@"FAIL");
}

@end
