//
//  CustomDetailCell.m
//  UsingStackoverflowAPI
//
//  Created by Vladislav on 06.12.13.
//  Copyright (c) 2013 High Tech Center. All rights reserved.
//

#import "CustomDetailCell.h"

@implementation CustomDetailCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
